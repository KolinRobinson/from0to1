"use strict"

let video = document.querySelector('#video'),
	videoButton = document.querySelector('.video_play');

videoButton.addEventListener('click', e => {
	e.preventDefault();
	video.play();
	video.setAttribute('controls', 'true')
	videoButton.hidden = true;
})

video.addEventListener('click', e => {
	e.preventDefault();
	if (videoButton.hidden){
		video.pause();
		video.removeAttribute('controls')
		videoButton.hidden = false;
	} else {
		return false
	}

})

new Swiper('.swiper-container', {
	speed: 500,
	loop: true,
	fadeEffect: {
		crossFade: true
	},
	autoplay: {
		delay: 3000,
	},
	pagination: {
		el: '.swiper-pagination',
		type: 'bullets',
		clickable: 'true'
	},

})

