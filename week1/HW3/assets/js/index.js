"use strict"

new Swiper('.swiper-container', {
	speed: 500,
	loop: true,
	effect: 'flip',
	autoplay: {
		delay: 3000,
	},
	pagination: {
		el: '.swiper-pagination',
		type: 'bullets',
		clickable: 'true'
	},

})